extends KinematicBody2D

export var speed = 200
export var jump_speed = 500
export var gravity = 20

var velocity = Vector2()

func _physics_process(_delta):
	velocity.x = (int(Input.is_action_pressed("right")) - int(Input.is_action_pressed("left"))) * speed
	
	velocity.y += gravity
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	if is_on_floor() and Input.is_action_pressed("jump"):
			velocity.y = -jump_speed
	
	if Input.is_action_pressed("right"):
		$Sprite.flip_h = false
	elif Input.is_action_pressed("left"):
		$Sprite.flip_h = true
	
	if is_on_floor():
		if Input.is_action_pressed("right") or Input.is_action_pressed("left"):
			$Sprite.play("run")
		else:
			$Sprite.play("idle")
	else:
		$Sprite.play("jump")
